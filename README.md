# Masters of Hardcore Top 100 2022 Live Broadcast

Data files regarding live broadcast, that took place on December 28th 12:00 midday (CET) on the Masters of Hardcore YouTube channel.

Official statement: https://www.mastersofhardcore.com/masters-of-hardcore-top-100-2022-voting-closed-broadcast-date-announced/

Broadcast Youtube link: https://www.youtube.com/watch?v=TH1XIONfoME

The project contains 1 dir with sfv, nfo & jpg files inside

## Contents

- [Masters.Of.Hardcore.Top.100.2022.Live.Broadcast.1080p.WEB.VP9](https://gitlab.com/1gabba/masters-of-hardcore-top-100-2022-broadcast/-/tree/main/Masters.Of.Hardcore.Top.100.2022.Live.Broadcast.1080p.WEB.VP9)

## Video content

All video content can be downloaded from [1gabba.pw](https://1gabba.pw):
- https://1gabba.pw/node/63126/thunderdome-2022-angerfist-tha-playah-1080p-web-x264


## Audio content

Audio stream rip can be downloaded from 1gabba.pw:

https://1gabba.pw/node/63126/thunderdome-2022-angerfist-tha-playah-1080p-web-x264

Masters Of Hardcore Top 100 2022 tracks are here:

`VA_-_Masters_Of_Hardcore_Top_100_2022-WEB-2022`

https://1gabba.pw/node/62809/va-masters-of-hardcore-top-100-2022-web-2022

Masters Of Hardcore 2022 Yearmix is here:

`Thunderdome_2022-Livesets-WEB-12-10-2022-CiN_INT`

Various_Artists-Masters_Of_Hardcore_2022_Yearmix-WEB-12-23-2022-CiN_INT


***
